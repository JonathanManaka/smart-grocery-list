# Smart List



## 📦 Packages

- flutter_riverpod- https://pub.dev/packages/flutter_riverpod
- intl - https://pub.dev/packages/intl
- google_fonts - https://pub.dev/packages/google_fonts
- auto_route - https://pub.dev/packages/auto_route
- video_player - https://pub.dev/packages/video_player
- flutter_native_splash - https://pub.dev/packages/flutter_native_splash
- auto_route_generator - https://pub.dev/packages/auto_route_generator
- flutter_launcher_icons - https://pub.dev/packages/flutter_launcher_icons

## 💣 Functionality

- Fully CRUD app (Creating list/item, Reading list/item, updating list/item, Deleting list/item)
- Grocery items calculator
- Comparing previous and current grocery items
- Animated initial loading splash screen



## 🚀 State Management / Structure

- Flutter riverpod

## 🎨 Design

- The design is inpired by the UI I created using figma.
    - https://www.figma.com/file/CY1c28OMjFtguh9Z54J4Cw/Smart-Shopping-List?type=design&node-id=0%3A1&mode=design&t=JjIBcfDMmfj3fTCO-1

    - font-family: Annie Use Your Telescope for title except for buttons and other places I used poppins
- color pallete: ![Alt text](<Color pallete.png>)
- icons are from: https://www.flaticon.com/
- Animated splash video is from: https://lottiefiles.com/


## 💡 Challenges

- I had a challenge with getting grocery lists form database - The problem was caused by using the same state to manage requests from db and creating new state by the user.


# Screenshot
![Alt text](<Frame 1.png>)

## Live preview


## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
